open SCaml

type config = 
  { title          : string
  ; beginning_time : timestamp
  ; finish_time    : timestamp
  }
        
type storage = 
  { config     : config
  ; candidates : (string, nat) map
  ; voters     : address set
  }

let [@entry] vote name storage =
  let now = Global.get_now () in

  assert (storage.config.beginning_time <= now 
          && now <= storage.config.finish_time);

  let addr = Global.get_source () in

  assert (not (Set.mem addr storage.voters));

  let voters' = Set.update addr true storage.voters in

  let n' = match Map.get name storage.candidates with
    | Some n -> n +^ Nat 1
    | None -> failwith "no such candidate"
  in

  let candidates' = Map.update name (Some n') storage.candidates in
  
  let storage' = 
    { config     = storage.config;
      candidates = candidates';
      voters     = voters'
    }
  in

  ([], storage')
