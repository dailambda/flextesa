(* counter.ml *)
open SCaml

let [@entry] main param (param_sum, counter) =
  ([], (param + param_sum, counter +^ Nat 1))
  
