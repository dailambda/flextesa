open Flextesa

let node =
  Tezos_node.make ~exec: (Tezos_executable.make `Node)
    "dummy_id"
    ~expected_connections: 1
    ~rpc_port: 28732
    ~p2p_port: 29732
    []

let client = Tezos_client.of_node ~exec:(Tezos_executable.make `Client) node
let console = Console.make true true
    
module I = Internal_pervasives
  
let pp_error ppf error =
  match error with
  | `Process_error _ as pe -> I.Process_result.Error.pp ppf pe
  | `System_error (`Fatal, _) as se -> I.System_error.pp ppf se

type 'a ar =
  ('a,  [ `Process_error of I.Process_result.Error.error | `System_error of [ `Fatal ] * I.System_error.static ]) I.Asynchronous_result.t

let pp_process_result ppf (pr: I.Process_result.t) =
  Format.fprintf ppf "{err=[%a], out=[%a], status=%s}"
    (Format.pp_print_list Format.pp_print_string) pr#err
    (Format.pp_print_list Format.pp_print_string) pr#out
    (I.Process_result.status_to_string pr#status)

let rec map_m ~f ms =
  let open I in
  match ms with
  | [] -> return []
  | m :: ms -> f m >>= fun y -> map_m ~f ms >>= fun ys -> return (y :: ys)
  
let get_balance state ~client name =
  let open I in
  Tezos_client.successful_client_cmd state ~client ["get"; "balance"; "for"; name] >>= fun res ->
  let num = Re.(rep1 (alt [digit; char '.'])) in
  let re =
    Re.(
      compile (seq [group num ; str " ꜩ"])
    )
  in
  match res#out with
  | [line] ->
    begin match Re.exec_opt re line with
      | Some matches ->
        Re.Group.get matches 1
        |> Float.of_string
        |> return
      | None ->
        fail (`Process_error (Process_result.Error.Wrong_behavior {message="regexp unreadable : "^line}))
    end
  | _ ->
    fail (`Process_error (Process_result.Error.Wrong_behavior {message="not one line"}))
  
let gen_keys state ~client name =
  Tezos_client.successful_client_cmd state ~client ["gen"; "keys"; name]

let list_known_contracts state ~client =
  let open I in
  Tezos_client.successful_client_cmd state ~client ["list"; "known"; "contracts"] >>= fun res ->
  let re =
    Re.(
      compile
        (seq
           [ group (rep1 (alt [alnum; char '_'; char '-']))
           ; str ": "
           ; group (rep1 alnum)
           ; alt [space; eol; eos] ])) in
  map_m res#out ~f:(fun line ->
      match Re.exec_opt re line with
      | Some matches ->
        return (Re.Group.get matches 1, Re.Group.get matches 2)
      | None ->
        fail (`Process_error (Process_result.Error.Wrong_behavior {message="known contracts parse error"})))
    
let string_of_amount amount = Printf.sprintf "%.000f" amount

let transfer state ~client ?arg amount ~from ~to_ ~burn_cap =
  let open I in
  let arg_param = match arg with
    | None -> []
    | Some a -> ["--arg"; a]
  in
  ["transfer"; string_of_amount amount; "from"; from; "to"; to_;
   "--burn-cap"; string_of_amount burn_cap;] @ arg_param
  |> Tezos_client.successful_client_cmd state ~client
  >>= fun res ->
  let re = Re.(compile (seq [str "      Updated storage: "; group (rep any)])) in
  match
    List.find_map ~f:(fun line -> Re.exec_opt re line) res#out
  with
  | Some matches -> Re.Group.get matches 1 |> return
  | None -> return ""

(* deploy smart contract *)
let originate_contract state ~client ~name ~amount ~from ?(init = "Unit") ~burn_cap tz_file =
  if not @@ Sys.file_exists tz_file then assert false;
  let open I in
  Tezos_client.successful_client_cmd state ~client ["originate"; "contract"; name;
                                                    "transferring"; string_of_amount amount; "from"; from;
                                                    "running"; tz_file;
                                                    "--init"; init;
                                                    "--burn-cap"; string_of_amount burn_cap] >>= fun _res ->
  return ()

let get_contract_storage state ~client contract =
  let open I in
  Tezos_client.successful_client_cmd state ~client ["get"; "contract"; "storage"; "for"; contract] >>= fun res ->
  return (String.concat ~sep:"\n" res#out)
  
let () =
  let state = object
    method application_name = "HOGE"
    method console = console
    method paths = Paths.make "dir"
    method env_config = Environment_configuration.default ()
    method runner= Running_processes.State.make ()
    method operations_log = Log_recorder.Operations.make ()
  end in
  let client = Tezos_client.of_node ~exec:(Tezos_executable.make `Client) node in
  let ar : _ ar =
    let open I in
    Running_processes.start state (Tezos_node.process state node) >>= fun _process_state ->
    let protocol = Tezos_protocol.default() in
    Tezos_protocol.ensure state protocol >>= fun () ->    
    Tezos_client.wait_for_node_bootstrap state client >>= fun () ->
    Tezos_client.activate_protocol state client protocol >>= fun () ->
    let bs = protocol.Tezos_protocol.bootstrap_accounts in
    let bootstrap1_acc = Caml.List.hd bs |> fst in
    let bootstrap1 = Tezos_protocol.Account.name bootstrap1_acc in
    let bootstrap1_private_key = Tezos_protocol.Account.private_key bootstrap1_acc in
    Tezos_client.import_secret_key state client ~name:bootstrap1 ~key:bootstrap1_private_key >>= fun () ->
    Tezos_client.get_block_header state ~client `Head >>= fun (header : Ezjsonm.value) ->
    let dictator = "dictator-default" in
    let myself = "myself" in
    get_balance state ~client dictator >>= fun before ->
    gen_keys state ~client myself >>= fun _res ->
    let burn_cap = 1. in
    let simplest = "simplest" in
    originate_contract state ~client ~name:simplest ~amount:0. ~from:dictator ~burn_cap "src/sample/scaml/simplest.tz" >>= fun () ->
    let keyed = Tezos_client.Keyed.make client ~key_name:bootstrap1 ~secret_key:bootstrap1_private_key in
    Tezos_client.Keyed.bake state keyed "ManualBaking" >>= fun () ->
    transfer state ~client ~from:dictator ~to_:myself ~burn_cap 1. >>= fun _res ->
    Tezos_client.Keyed.bake state keyed "ManualBaking" >>= fun () ->
    (* counter contract *)
    let counter = "counter" in
    let init = "(Pair 0 0)" in
    originate_contract state ~client ~name:counter ~amount:0. ~from:dictator ~init ~burn_cap "src/sample/scaml/counter.tz" >>= fun () ->
    Tezos_client.Keyed.bake state keyed "ManualBaking" >>= fun () ->
    transfer state ~client ~from:dictator ~to_:counter ~burn_cap ~arg:"42" 0. >>= fun _ ->
    Tezos_client.Keyed.bake state keyed "ManualBaking" >>= fun () ->
    get_contract_storage state ~client counter >>= fun updated_counter_storage ->

    (* vote contract *)
    let vote = "vote" in
    let init =
      {| Pair (Pair "senkyo" (Pair "2020-04-01T00:00:00-00:00" "2020-05-01T00:00:00-00:00")) (Pair { Elt "shinzo" 0 ; Elt "taro" 0 } {}) |}
    in
    let burn_cap = 10. in
    originate_contract state ~client ~name:vote ~amount:0. ~from:dictator ~init ~burn_cap "src/sample/scaml/vote.tz" >>= fun () ->
    Tezos_client.Keyed.bake state keyed "ManualBaking" >>= fun () ->
    transfer state ~client ~from:dictator ~to_:vote ~burn_cap ~arg:{|"shinzo"|} 0. >>= fun _ ->
    Tezos_client.Keyed.bake state keyed "ManualBaking" >>= fun () ->
    get_contract_storage state ~client vote >>= fun updated_vote_storage ->
    
    Tezos_client.list_known_addresses state ~client >>= fun known_addresses ->
    list_known_contracts state ~client >>= fun known_contracts ->
    get_balance state ~client dictator >>= fun after ->
    return (header, known_addresses, protocol, known_contracts, before, after, bs, updated_counter_storage, updated_vote_storage)
  in
  let pp_ok ppf (_header, known_addresses, protocol, known_contracts, before, after, bs, updated_counter_storage, updated_vote_storage) =
    let pp_addr ppf (k, v) = Format.fprintf ppf "(%s, %s)" k v in
    let pp_boots ppf (account, i) = Format.fprintf ppf "{name=%s, private_key=%s, i=%Ld}" (Tezos_protocol.Account.name account) (Tezos_protocol.Account.private_key account) i in
    Format.fprintf ppf "OK header=, known_addrs=[%a], res of cmd = @, protocol = %s, known_contracts=[%a], amounts: %f -> %f, bootstrapp accoutns=[%a], counter storage = '%s', vote storage = '%s'"
      (Format.pp_print_list pp_addr) known_addresses
      (Tezos_protocol.id protocol)
      (Format.pp_print_list pp_addr) known_contracts
      before after
      (Format.pp_print_list pp_boots) bs
      updated_counter_storage
      updated_vote_storage
  in
  Lwt_main.run
  @@ Lwt.bind ar (fun atr ->
    Format.eprintf "atr=%a@."
      (Internal_pervasives.Attached_result.pp ~pp_ok ~pp_error) atr;
    Lwt.return ())
