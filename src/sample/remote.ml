open Flextesa

let client = Tezos_client.of_remote 
    ~id:"carthagenet.tezos.cryptium.ch"
    ~exec:(Tezos_executable.make `Client) 
    ~host:"carthagenet.tezos.cryptium.ch"
    ~port: 8732

let console = Console.make true true
    
module I = Internal_pervasives
  
let pp_error ppf error =
  match error with
  | `Process_error _ as pe -> I.Process_result.Error.pp ppf pe
  | `System_error (`Fatal, _) as se -> I.System_error.pp ppf se

type 'a ar =
  ('a,  [ `Process_error of I.Process_result.Error.error | `System_error of [ `Fatal ] * I.System_error.static ]) I.Asynchronous_result.t

let pp_process_result ppf (pr: I.Process_result.t) =
  Format.fprintf ppf "{err=[%a], out=[%a], status=%s}"
    (Format.pp_print_list Format.pp_print_string) pr#err
    (Format.pp_print_list Format.pp_print_string) pr#out
    (I.Process_result.status_to_string pr#status)

let () =
  let state = object
    method application_name = "HOGE"
    method console = console
    method paths = Paths.make "dir"
    method env_config = Environment_configuration.default ()
    method runner= Running_processes.State.make ()
  end in
  let ar : _ ar =
    let open I in

    (* XXX
       * Make sure tezos-node and tezos-client are in PATH!
       * Remove "dir" to remove files of the previous tests!
    *)

    (* We need protocol parameters!!!, otherwise, tezos_node process exists
       earlier, and Running_processes.start does not catch the error.
    *)
    (* We have to wait the node start responding.  Otherwise, the later
       operations fail to communicate the node *)
    Caml.prerr_endline "waiting bootstrapped";
    Tezos_client.wait_for_node_bootstrap state client  >>= fun () ->
    Caml.prerr_endline "bootstrapped";

(*
    Caml.prerr_endline "activate alpha";
    let protocol = Tezos_protocol.default () in
    Tezos_client.activate_protocol state client protocol >>= fun () ->
*)

    Tezos_client.get_block_header state ~client `Head >>= fun (header : Ezjsonm.value) ->
    Caml.prerr_endline "header done";

    Tezos_client.client_cmd state ~client ["get"; "balance"; "for"; "dictator-default"] >>= fun (_b, pr) ->
    Caml.prerr_endline "balance done";

    Tezos_client.list_known_addresses state ~client >>= fun known_addresses ->
    return (header, known_addresses, pr)
  in
  let pp_ok ppf (header, known_addresses, pr) =
    let pp_addr ppf (k, v) = Format.fprintf ppf "(%s, %s)" k v in
    Format.fprintf ppf "OK header=%s, know_addrs=[%a], res of cmd = %a@." (Ezjsonm.value_to_string ~minify:false header)
      (Format.pp_print_list pp_addr) known_addresses
      pp_process_result pr
  in
  Lwt_main.run
  @@ Lwt.bind ar (fun atr ->
    Format.eprintf "atr=%a@."
      (Internal_pervasives.Attached_result.pp ~pp_ok ~pp_error) atr;
    Lwt.return ())
